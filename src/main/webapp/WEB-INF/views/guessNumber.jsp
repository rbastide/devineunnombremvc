<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Devine un nombre</title>
	</head>
	<body onload="document.guessForm.guess.focus()">
		<h3>Hello ${player.playerName}, devine mon nombre</h3>
		<c:if test="${game.attempts > 0}">
			<!-- Si on a fait au moins un essai -->
			Essai n° <b>${game.attempts}</b><br/>
			${game.guess} : <b>
			<c:choose>
				<c:when test="${game.guess > game.target}">
					Trop haut !
				</c:when>
				<c:otherwise>
					Trop bas !
				</c:otherwise>
			</c:choose>									
			</b>	
		</c:if>
		<h2>je pense à un nombre compris entre 0 et ${game.max}</h2>
		<form name="guessForm" method="POST" accept-charset="UTF-8" action="guess">
			<label>Ta proposition : <input type="number" min="0" max="100" required name="guess"></label> 
			<input type="SUBMIT" value="Deviner"><br/>
		</form>
		<a href="logout">Déconnexion</a>
		<hr>
		<c:if test="${not empty application.bestPlayerName}">
			<h2>Score à battre : ${application.bestScore} essais par ${application.bestPlayerName}</h2>
		</c:if>		
	</body>
</html>
