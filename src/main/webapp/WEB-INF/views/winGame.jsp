<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Devine un nombre</title>
	</head>
	<body>
		<h3>Bravo ${player.playerName}, tu as gagné en ${game.attempts} coups !</h3>
		<a href="playAgain">Rejouer</a>
		<a href="logout">Déconnexion</a>
	</body>
</html>
