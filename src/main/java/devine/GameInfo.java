package devine;


import java.io.Serializable;
import java.util.Random;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@SessionScoped
@Named("game")
public class GameInfo implements Serializable {
	private static final int MAX = 100;
	// Un générateur aléatoire
	Random generator = new Random(System.nanoTime());
	
	private int target = -1;
	private int attempts = 0;
	private int guess = 0;
	
	public void start() {
		target = generator.nextInt(MAX + 1);
		attempts = 0;
		guess = -1;
	}

	public void finishGame() {
		this.target = -1 ;
		this.attempts = 0;	
		guess = 0;
	}
	
	public boolean isFinished() {
		return guess == target;
	}
	
	public int getAttempts() {
		return attempts;
	}
	
	public void newGuess(int guess) {
		attempts++;
		this.guess = guess;
	}
	
	// Accesseurs
	
	public int getGuess() {
		return guess;
	}

	public int getTarget() {
		return target;
	}
	
	public int getMax() {
		return MAX;
	}
}
