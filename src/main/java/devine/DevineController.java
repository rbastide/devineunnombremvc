package devine;

import java.util.Random;
import javax.inject.Inject;
import javax.mvc.Controller;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

@Controller @Path("/")
public class DevineController {
	@Inject // Meilleur score, Application scoped
	private BestScoreInfo bestScore;
	@Inject // Les infos du joueur, Session scoped
	private PlayerInfo player;
	@Inject // Les infos du jeu en cours, Session scoped
	private GameInfo game;
	@GET
	public String showView() {
		if (!player.isConnected()) {
			return "loginForm.jsp";
		}

		if (game.isFinished()) {
			return "winGame.jsp";
		}

		return "guessNumber.jsp";
	}
	@POST @Path("guess")
	public String guessNumber(@FormParam("guess") int guess) {
		if (player.isConnected()) {
			game.newGuess(guess);
			if (game.isFinished()) {
				bestScore.updateBestScore(player, game);
			}
		}
		return "redirect:/";
	}	
	@GET @Path("logout")
	public String logout() {
		player.logout();
		return "redirect:/";
	}	
	@POST @Path("login")
	public String login(@FormParam("playerName") String playerName) {
		player.login(playerName);
		return playNewGame();
	}

	@GET @Path("playAgain")
	public String playNewGame() {
		if (player.isConnected()) {
			game.start();
		}
		return "redirect:/";
	}
}
