package devine;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

@ApplicationScoped
@Named("application")
public class BestScoreInfo {	
	private String bestPlayerName = null;
	
	private int bestScore = Integer.MAX_VALUE;

	public int getBestScore() {
		return bestScore;
	}

	public String getBestPlayerName() {
		return bestPlayerName;
	}
	
	public void updateBestScore(PlayerInfo player, GameInfo game) {
		if (game.getAttempts() < bestScore) {
			bestScore = game.getAttempts();
			bestPlayerName = player.getPlayerName();
		}
	}

}
