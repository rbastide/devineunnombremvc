package devine;


import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@SessionScoped
@Named("player")
public class PlayerInfo implements Serializable {
	
	private String playerName = null;
	
	public void login(String name) {
		this.playerName = name;
	}
	public void logout() {
		this.playerName = null;
	}
		public boolean isConnected() {
		return playerName != null;
	}
	public String getPlayerName() {
		return playerName;
	}
}
